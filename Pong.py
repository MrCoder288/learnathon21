import turtle as t


win = t.Screen()
win.bgcolor('blue')
win.setup(width=800, height=600)
win.tracer(0)


paddle_left = t.Turtle()
paddle_left.speed(0)
paddle_left.shape('square')
paddle_left.color('grey')
paddle_left.shapesize(stretch_wid=5, stretch_len=1)
paddle_left.penup()
paddle_left.goto(-350, 0)


paddle_right = t.Turtle()
paddle_right.speed(0)
paddle_right.shape('square')
paddle_right.shapesize(stretch_wid=5, stretch_len=1)
paddle_right.color('grey')
paddle_right.penup()
paddle_right.goto(350, 0)


ball = t.Turtle()
ball.speed(1)
ball.shape('circle')
ball.color('white')
ball.penup()
ball.goto(0, 0)
ball_dx = 0.5
ball_dy = 0.5



def paddle_left_up():
    y = paddle_left.ycor()
    y = y + 15
    paddle_left.sety(y)



def paddle_left_down():
    y = paddle_left.ycor()
    y = y - 15
    paddle_left.sety(y)



def paddle_right_up():
    y = paddle_right.ycor()
    y = y + 15
    paddle_right.sety(y)



def paddle_right_down():
    y = paddle_right.ycor()
    y = y - 15
    paddle_right.sety(y)



win.listen()
win.onkeypress(paddle_left_up, "w")
win.onkeypress(paddle_left_down, "s")
win.onkeypress(paddle_right_up, "Up")
win.onkeypress(paddle_right_down, "Down")


while True:
    win.update()

    ball.setx(ball.xcor() + ball_dx)
    ball.sety(ball.ycor() + ball_dy)


    if ball.ycor() > 290:
        ball.sety(290)
        ball_dy = ball_dy * -1

        if ball.ycor() < -290:
            ball.sety(-290)
            ball_dy = ball_dy * -1

    if ball.xcor() > 390:
        ball.goto(0, 0)
        ball_dx = ball_dx * -1

    if (ball.xcor()) < -390:
        ball.goto(0, 0)
        ball_dx = ball_dx * -1


    if (ball.xcor() > 340) and (ball.xcor() < 350) and (
            ball.ycor() < paddle_right.ycor() + 40 and ball.ycor() > paddle_right.ycor() - 40):
        ball.setx(340)
        ball_dx = ball_dx * -1


    if (ball.xcor() < -340) and (ball.xcor() > -350) and (
            ball.ycor() < paddle_left.ycor() + 40 and ball.ycor() > paddle_left.ycor() - 40):
        ball.setx(-340)
        ball_dx = ball_dx * -1